import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

@WebServlet("/")
public class MyServlet extends HttpServlet {

    private List<Task> taskList = new ArrayList<>();
    private final static String baseFile = "task.json";


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        super.doPost(request, response);

        Map<String, String[]> map = request.getParameterMap();

        String name = "";
        String description = "";
        String done = "";
        String id = "";
        Integer mode = 0;

        //Обработка приходящего post запроса
        for (String paramName : map.keySet()) {

            /**
             * Сложная логика которую сложно поддерживать
             *
             * 1. Заведи параметр action или mode который передавай из index.jsp
             * 2. Соотвественно исходя из него тебе надо определять все ли параметры были переданы их фронта что бы обработать действие или режим
             * 3. Ну и как следствие нужно возвращать результат ответа на страницу и валидно его обрабатывать.
             *
             */

            Request requestObject = toJavaObjectRequest(paramName);

            switch (requestObject.getMode()) {
                case "add":
                    System.out.println("Зашел в Add");
                    taskList.add(new Task(taskList.size() + 1, requestObject.getTitle(), requestObject.getDescription(), false, false));
                    toJSON(taskList);
                    System.out.println("Добавил объект");
                    response.setStatus(200);
                    break;
                case "updateDone":
                    System.out.println("Поменял маркер выполнения");
                    markAsDone(Integer.parseInt(requestObject.getId()) - 1, Boolean.parseBoolean(requestObject.getDone()));
                    break;
                case "updateToArchive":
                    System.out.println("Поместил в архив");
                    putIntoArchive(Integer.parseInt(requestObject.getId()) - 1);
                    break;
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");

        //Обработка приходящего get запроса

        //Изначальная проблема,
        //Так будет лучше, другой вариант проверять на NPE
        //вообще нужно исходить из того что пользователь
        //не знает что в твое приложение надо заходить как
        //http://localhost:8080/maven-webapp/?archive=true
        boolean predicate = "true".equalsIgnoreCase(request.getParameter("archive"));

        List<Task> result = new ArrayList<>();
        for (Task task : taskList = toJavaObject()) {
            if (predicate == task.getArchived()) {
                result.add(task);
            }
        }

        request.setAttribute("taskList", result);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/index.jsp");
        dispatcher.forward(request, response);

    }

    //Хранение данных в файле json
    private static void toJSON(List<Task> taskList) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(new File(baseFile), taskList);
        System.out.println("json created!");
    }

    private static Request toJavaObjectRequest(String paramName) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(paramName, new TypeReference<Request>() {
        });
    }

    private static List<Task> toJavaObject() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(new File(baseFile), new TypeReference<List<Task>>() {
        });
    }

    //Логика изменения состояния задач
    private void markAsDone(Integer id, Boolean done) throws IOException {
            taskList.set(id, new Task(id + 1, taskList.get(id).getTitle(), taskList.get(id).getDescription(), done,
                    taskList.get(id).getArchived()));
            toJSON(taskList);
    }

    private void putIntoArchive(Integer id) throws IOException {
        if (taskList.get(id).getDone().equals(true)) {
            if (taskList.get(id).getArchived().equals(false)) {
                taskList.set(id, new Task(id + 1, taskList.get(id).getTitle(), taskList.get(id).getDescription(),
                        taskList.get(id).getDone(), true));
            } else {
                taskList.set(id, new Task(id + 1, taskList.get(id).getTitle(), taskList.get(id).getDescription(),
                        taskList.get(id).getDone(), false));
            }
            toJSON(taskList);
        }
    }
}