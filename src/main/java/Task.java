import com.fasterxml.jackson.annotation.*;

public class Task {

    @JsonProperty("Id")
    private Integer id;
    @JsonProperty("Title")
    private String title;
    @JsonProperty("Description")
    private String description;
    @JsonProperty("Done")
    private Boolean done;
    @JsonProperty("Archived")
    private Boolean archived;

    public Task (){
    }

    public Task(Integer id, String title, String description, Boolean done, Boolean archived) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.done = done;
        this.archived = archived;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

    public Boolean getArchived() {
        return archived;
    }

    public void setArchived(Boolean archived) {
        this.archived = archived;
    }

    @Override
    public String toString() {
        return "Task[ID:" + id + ", Title: " + title + ", Description: " + description + ", Done: " + done + ", Archived: " + archived + "]";
    }
}