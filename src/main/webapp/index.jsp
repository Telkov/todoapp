<!--

1. Обработка ошибок
2. Успешно или нет было отправлено уведомление в архив

-->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<script src="http://code.jquery.com/jquery-2.2.4.js" type="text/javascript"></script>
<html>
<head>
    <title></title>
</head>
<body>
<h3 style="margin-left: 175px" id="currentMode">Задачи</h3>
<div style="margin-left: 40px">
    <table cellspacing="1" cellpadding="4" border="3" id="infoTable">
        <tr>
            <th>#</th>
            <th>Задача</th>
            <th>Описание</th>
            <th>Выполнено?</th>
            <th>Архив</th>
        </tr>

        <jsp:useBean id="taskList" scope="request" type="java.util.List"/>
        <c:forEach var="task" items="${taskList}">
            <tr id="rowId${task.id}" name="row">
                <td style="text-align: center">${task.id}</td>
                <td style="text-align: center">${task.title}</td>
                <td style="text-align: center">${task.description}</td>
                <td style="text-align: center">
                    <label>
                        <input type="checkbox" onclick=""
                               <c:if test="${task.done}">checked=checked</c:if>/>
                    </label>
                </td>
                <td style="text-align: center">
                    <button name="putIntoArchive" onclick=""><></button>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>

<div style="margin-top: 20px">
    <label for="title"></label><input id="title" type="text" placeholder="title">
    <label for="description"></label><input id="description" type="text" placeholder="description">
    <button id="addTask" onclick="">Добавить</button>
</div>
<div style="margin-left: 125px; margin-top: 25px; margin-bottom: 25px">
    <div style="float: left;">
        <form action="MyServlet" method="get">
            <button type="submit" name="archive" value="false">Текущие</button>
        </form>
    </div>
    <div style="float: left; margin-left: 25px">
        <form action="MyServlet" method="get">
            <button type="submit" name="archive" value="true">Архив</button>
        </form>
    </div>
</div>
</body>

<script type="text/javascript">
    $(document).ready(function () {
        $('#addTask').click(function () {
            var title = $('#title').val();
            var description = $('#description').val();
            var myData = {
                    "Mode": "add",
                    "Title": title,
                    "Description": description
            };
            if (title === "" || description === "") {
                alert("Введите название и описание задачи!");
            } else {
                alert("Вы добаввили задачу: " + title);
                $.ajax({
                    type: "post",
                    url: "/maven-webapp/",
                    data: JSON.stringify(myData)
                });

            }
            location.href = ''
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("input[type='checkbox']").click(function () {
            var row = $(this).parent().parent().parent().find('td:first').text();
            var myData = {
                "Mode": "updateDone",
                "Id": row,
                "Done": $(this).is(":checked")
            };
            $.ajax({
                type: "post",
                url: "/maven-webapp/",
                data: JSON.stringify(myData)
            });
            location.href = ''
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("button[name='putIntoArchive']").click(function () {
            var row = $(this).parent().parent().find('td:first').text();
            // if (row.find('input[type=\'checkbox\']').is(":checked") === false){
            //     alert("Эта задача еще не выполнена!");
            // }
            var myData = {
                "Mode": "updateToArchive",
                "Id": row
            };
            $.ajax({
                type: "post",
                url: "/maven-webapp/",
                data: JSON.stringify(myData)
            });
            location.href = ''
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#archivedTasks').click(function () {
            $("#currentMode").value("your new header");
            $.ajax({
                type: "post",
                url: "/maven-webapp/",
                data: "Archive"
            });
            location.href = ''
        });
    });
</script>

</html>